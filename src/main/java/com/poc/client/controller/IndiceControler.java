package com.poc.client.controller;

import com.poc.client.client.IndiceClient;
import com.poc.ws.indiceswsdl.AllIndices;
import com.poc.ws.indiceswsdl.AllIndicesResponse;
import com.poc.ws.indiceswsdl.Indice;
import com.poc.ws.indiceswsdl.ObjectFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController public class IndiceControler {

	private IndiceClient indiceClient;

	public IndiceControler(IndiceClient indiceClient) {
		this.indiceClient = indiceClient;

	}

	@GetMapping(value = "/indices", produces = MediaType.APPLICATION_JSON_VALUE) public Object searchIndices(
			@RequestBody AllIndices indice) {

		Object allIndices = indiceClient.getAllIndices(indice);
		return allIndices;
	}

}
