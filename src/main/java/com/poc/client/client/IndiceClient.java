package com.poc.client.client;

import br.com.projuris.indices.service.IndiceServiceWSimpl;
import br.com.projuris.indices.service.IndiceWS;
import com.poc.ws.indiceswsdl.*;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

@Component
public class IndiceClient {

	private final WebServiceTemplate webServiceTemplate;
	private final IndiceWS service;
	public IndiceClient(WebServiceTemplate webServiceTemplate, IndiceWS service) {
		this.webServiceTemplate = webServiceTemplate;
		this.service = service;
	}

	public Object getAllIndices(AllIndices indice) {

		IndiceServiceWSimpl indiceServiceWSimplPort = service.getIndiceServiceWSimplPort();

		Object o = webServiceTemplate.marshalSendAndReceive("http://localhost:9090/wsdl/indices", indice);
		return o;
	}

}
