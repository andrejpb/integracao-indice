package com.poc.client.config;

import br.com.projuris.indices.service.IndiceWS;
import com.poc.ws.indiceswsdl.ObjectFactory;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import java.net.MalformedURLException;
import java.net.URL;

@Configuration public class SoapConfig {
	@Value("${client.default-uri}") private String defaultUri;

	@Value("${client.user.name}") private String userName;

	@Value("${client.user.password}") private String userPassword;

	@Bean public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
		jaxb2Marshaller.setPackagesToScan(ObjectFactory.class.getPackage().getName());
		return jaxb2Marshaller;
	}

	@Bean public WebServiceTemplate webServiceTemplate() {
		Jaxb2Marshaller marshaller = jaxb2Marshaller();
		WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
		webServiceTemplate.setDefaultUri(defaultUri);
		webServiceTemplate.setMarshaller(marshaller);
		webServiceTemplate.setUnmarshaller(marshaller);
		webServiceTemplate.setMessageSender(defaultMwMessageSender());
		return webServiceTemplate;
	}

	@Bean public HttpComponentsMessageSender defaultMwMessageSender() {
		HttpComponentsMessageSender messageSender = new HttpComponentsMessageSender();
		messageSender.setCredentials(new UsernamePasswordCredentials(getUserName(), getUserPassword()));
		return messageSender;
	}

	@Bean public IndiceWS getIndiceWS() throws MalformedURLException {
		return new IndiceWS(new URL(defaultUri));

	}

	public String getUserName() {
		return userName;
	}

	public String getUserPassword() {
		return userPassword;
	}
}
