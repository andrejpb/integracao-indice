package com.poc.client.api;

import br.com.projuris.indices.service.Indice;
import br.com.projuris.indices.service.IndiceException;
import br.com.projuris.indices.service.IndiceServiceWSimpl;
import br.com.projuris.indices.service.IndiceWS;
import com.poc.ws.indiceswsdl.AllIndices;
import com.poc.ws.indiceswsdl.AllIndicesResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint("/indices")
public class ClientEndPoint {
	private static final String NAMESPACE_URI = "http://service.indices.projuris.com.br/";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "allIndices")
	@ResponsePayload
	public List<Indice> getIndices(@RequestPayload AllIndices request) throws IndiceException {
		AllIndicesResponse response = new AllIndicesResponse();
		IndiceWS service = new IndiceWS();
		IndiceServiceWSimpl indiceServiceWSimplPort = service.getIndiceServiceWSimplPort();

		return indiceServiceWSimplPort.allIndices();
	}
}
