
package br.com.projuris.indices.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.projuris.indices.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NextIndicesByAnoMesResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "nextIndicesByAnoMesResponse");
    private final static QName _IndicesByAnoMesResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByAnoMesResponse");
    private final static QName _IndicesByDescricaoAnoMesResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByDescricaoAnoMesResponse");
    private final static QName _NextIndicesByDescricaoAnoMesResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "nextIndicesByDescricaoAnoMesResponse");
    private final static QName _NextIndice_QNAME = new QName("http://service.indices.projuris.com.br/", "nextIndice");
    private final static QName _NextIndicesByDescricaoAnoMes_QNAME = new QName("http://service.indices.projuris.com.br/", "nextIndicesByDescricaoAnoMes");
    private final static QName _IndicesByAno_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByAno");
    private final static QName _AllIndices_QNAME = new QName("http://service.indices.projuris.com.br/", "allIndices");
    private final static QName _AllIndicesResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "allIndicesResponse");
    private final static QName _DescricaoIndicesResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "descricaoIndicesResponse");
    private final static QName _DescricaoIndices_QNAME = new QName("http://service.indices.projuris.com.br/", "descricaoIndices");
    private final static QName _IndicesByDescricaoResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByDescricaoResponse");
    private final static QName _IndicesByDescricao_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByDescricao");
    private final static QName _NextIndicesByAnoMes_QNAME = new QName("http://service.indices.projuris.com.br/", "nextIndicesByAnoMes");
    private final static QName _IndicesByAnoMes_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByAnoMes");
    private final static QName _IndicesByDescricaoAnoMes_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByDescricaoAnoMes");
    private final static QName _IndicesByAnoResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "indicesByAnoResponse");
    private final static QName _IndiceException_QNAME = new QName("http://service.indices.projuris.com.br/", "IndiceException");
    private final static QName _NextIndiceResponse_QNAME = new QName("http://service.indices.projuris.com.br/", "nextIndiceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.projuris.indices.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IndicesByAno }
     * 
     */
    public IndicesByAno createIndicesByAno() {
        return new IndicesByAno();
    }

    /**
     * Create an instance of {@link NextIndice }
     * 
     */
    public NextIndice createNextIndice() {
        return new NextIndice();
    }

    /**
     * Create an instance of {@link NextIndicesByDescricaoAnoMes }
     * 
     */
    public NextIndicesByDescricaoAnoMes createNextIndicesByDescricaoAnoMes() {
        return new NextIndicesByDescricaoAnoMes();
    }

    /**
     * Create an instance of {@link AllIndices }
     * 
     */
    public AllIndices createAllIndices() {
        return new AllIndices();
    }

    /**
     * Create an instance of {@link NextIndicesByAnoMesResponse }
     * 
     */
    public NextIndicesByAnoMesResponse createNextIndicesByAnoMesResponse() {
        return new NextIndicesByAnoMesResponse();
    }

    /**
     * Create an instance of {@link NextIndicesByDescricaoAnoMesResponse }
     * 
     */
    public NextIndicesByDescricaoAnoMesResponse createNextIndicesByDescricaoAnoMesResponse() {
        return new NextIndicesByDescricaoAnoMesResponse();
    }

    /**
     * Create an instance of {@link IndicesByAnoMesResponse }
     * 
     */
    public IndicesByAnoMesResponse createIndicesByAnoMesResponse() {
        return new IndicesByAnoMesResponse();
    }

    /**
     * Create an instance of {@link IndicesByDescricaoAnoMesResponse }
     * 
     */
    public IndicesByDescricaoAnoMesResponse createIndicesByDescricaoAnoMesResponse() {
        return new IndicesByDescricaoAnoMesResponse();
    }

    /**
     * Create an instance of {@link IndicesByAnoResponse }
     * 
     */
    public IndicesByAnoResponse createIndicesByAnoResponse() {
        return new IndicesByAnoResponse();
    }

    /**
     * Create an instance of {@link NextIndiceResponse }
     * 
     */
    public NextIndiceResponse createNextIndiceResponse() {
        return new NextIndiceResponse();
    }

    /**
     * Create an instance of {@link FaultBean }
     * 
     */
    public FaultBean createFaultBean() {
        return new FaultBean();
    }

    /**
     * Create an instance of {@link DescricaoIndices }
     * 
     */
    public DescricaoIndices createDescricaoIndices() {
        return new DescricaoIndices();
    }

    /**
     * Create an instance of {@link IndicesByDescricaoResponse }
     * 
     */
    public IndicesByDescricaoResponse createIndicesByDescricaoResponse() {
        return new IndicesByDescricaoResponse();
    }

    /**
     * Create an instance of {@link AllIndicesResponse }
     * 
     */
    public AllIndicesResponse createAllIndicesResponse() {
        return new AllIndicesResponse();
    }

    /**
     * Create an instance of {@link DescricaoIndicesResponse }
     * 
     */
    public DescricaoIndicesResponse createDescricaoIndicesResponse() {
        return new DescricaoIndicesResponse();
    }

    /**
     * Create an instance of {@link IndicesByAnoMes }
     * 
     */
    public IndicesByAnoMes createIndicesByAnoMes() {
        return new IndicesByAnoMes();
    }

    /**
     * Create an instance of {@link IndicesByDescricaoAnoMes }
     * 
     */
    public IndicesByDescricaoAnoMes createIndicesByDescricaoAnoMes() {
        return new IndicesByDescricaoAnoMes();
    }

    /**
     * Create an instance of {@link IndicesByDescricao }
     * 
     */
    public IndicesByDescricao createIndicesByDescricao() {
        return new IndicesByDescricao();
    }

    /**
     * Create an instance of {@link NextIndicesByAnoMes }
     * 
     */
    public NextIndicesByAnoMes createNextIndicesByAnoMes() {
        return new NextIndicesByAnoMes();
    }

    /**
     * Create an instance of {@link Indice }
     * 
     */
    public Indice createIndice() {
        return new Indice();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NextIndicesByAnoMesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "nextIndicesByAnoMesResponse")
    public JAXBElement<NextIndicesByAnoMesResponse> createNextIndicesByAnoMesResponse(NextIndicesByAnoMesResponse value) {
        return new JAXBElement<NextIndicesByAnoMesResponse>(_NextIndicesByAnoMesResponse_QNAME, NextIndicesByAnoMesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByAnoMesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByAnoMesResponse")
    public JAXBElement<IndicesByAnoMesResponse> createIndicesByAnoMesResponse(IndicesByAnoMesResponse value) {
        return new JAXBElement<IndicesByAnoMesResponse>(_IndicesByAnoMesResponse_QNAME, IndicesByAnoMesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByDescricaoAnoMesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByDescricaoAnoMesResponse")
    public JAXBElement<IndicesByDescricaoAnoMesResponse> createIndicesByDescricaoAnoMesResponse(IndicesByDescricaoAnoMesResponse value) {
        return new JAXBElement<IndicesByDescricaoAnoMesResponse>(_IndicesByDescricaoAnoMesResponse_QNAME, IndicesByDescricaoAnoMesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NextIndicesByDescricaoAnoMesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "nextIndicesByDescricaoAnoMesResponse")
    public JAXBElement<NextIndicesByDescricaoAnoMesResponse> createNextIndicesByDescricaoAnoMesResponse(NextIndicesByDescricaoAnoMesResponse value) {
        return new JAXBElement<NextIndicesByDescricaoAnoMesResponse>(_NextIndicesByDescricaoAnoMesResponse_QNAME, NextIndicesByDescricaoAnoMesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NextIndice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "nextIndice")
    public JAXBElement<NextIndice> createNextIndice(NextIndice value) {
        return new JAXBElement<NextIndice>(_NextIndice_QNAME, NextIndice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NextIndicesByDescricaoAnoMes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "nextIndicesByDescricaoAnoMes")
    public JAXBElement<NextIndicesByDescricaoAnoMes> createNextIndicesByDescricaoAnoMes(NextIndicesByDescricaoAnoMes value) {
        return new JAXBElement<NextIndicesByDescricaoAnoMes>(_NextIndicesByDescricaoAnoMes_QNAME, NextIndicesByDescricaoAnoMes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByAno }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByAno")
    public JAXBElement<IndicesByAno> createIndicesByAno(IndicesByAno value) {
        return new JAXBElement<IndicesByAno>(_IndicesByAno_QNAME, IndicesByAno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllIndices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "allIndices")
    public JAXBElement<AllIndices> createAllIndices(AllIndices value) {
        return new JAXBElement<AllIndices>(_AllIndices_QNAME, AllIndices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllIndicesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "allIndicesResponse")
    public JAXBElement<AllIndicesResponse> createAllIndicesResponse(AllIndicesResponse value) {
        return new JAXBElement<AllIndicesResponse>(_AllIndicesResponse_QNAME, AllIndicesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DescricaoIndicesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "descricaoIndicesResponse")
    public JAXBElement<DescricaoIndicesResponse> createDescricaoIndicesResponse(DescricaoIndicesResponse value) {
        return new JAXBElement<DescricaoIndicesResponse>(_DescricaoIndicesResponse_QNAME, DescricaoIndicesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DescricaoIndices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "descricaoIndices")
    public JAXBElement<DescricaoIndices> createDescricaoIndices(DescricaoIndices value) {
        return new JAXBElement<DescricaoIndices>(_DescricaoIndices_QNAME, DescricaoIndices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByDescricaoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByDescricaoResponse")
    public JAXBElement<IndicesByDescricaoResponse> createIndicesByDescricaoResponse(IndicesByDescricaoResponse value) {
        return new JAXBElement<IndicesByDescricaoResponse>(_IndicesByDescricaoResponse_QNAME, IndicesByDescricaoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByDescricao }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByDescricao")
    public JAXBElement<IndicesByDescricao> createIndicesByDescricao(IndicesByDescricao value) {
        return new JAXBElement<IndicesByDescricao>(_IndicesByDescricao_QNAME, IndicesByDescricao.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NextIndicesByAnoMes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "nextIndicesByAnoMes")
    public JAXBElement<NextIndicesByAnoMes> createNextIndicesByAnoMes(NextIndicesByAnoMes value) {
        return new JAXBElement<NextIndicesByAnoMes>(_NextIndicesByAnoMes_QNAME, NextIndicesByAnoMes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByAnoMes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByAnoMes")
    public JAXBElement<IndicesByAnoMes> createIndicesByAnoMes(IndicesByAnoMes value) {
        return new JAXBElement<IndicesByAnoMes>(_IndicesByAnoMes_QNAME, IndicesByAnoMes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByDescricaoAnoMes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByDescricaoAnoMes")
    public JAXBElement<IndicesByDescricaoAnoMes> createIndicesByDescricaoAnoMes(IndicesByDescricaoAnoMes value) {
        return new JAXBElement<IndicesByDescricaoAnoMes>(_IndicesByDescricaoAnoMes_QNAME, IndicesByDescricaoAnoMes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndicesByAnoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "indicesByAnoResponse")
    public JAXBElement<IndicesByAnoResponse> createIndicesByAnoResponse(IndicesByAnoResponse value) {
        return new JAXBElement<IndicesByAnoResponse>(_IndicesByAnoResponse_QNAME, IndicesByAnoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "IndiceException")
    public JAXBElement<FaultBean> createIndiceException(FaultBean value) {
        return new JAXBElement<FaultBean>(_IndiceException_QNAME, FaultBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NextIndiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.indices.projuris.com.br/", name = "nextIndiceResponse")
    public JAXBElement<NextIndiceResponse> createNextIndiceResponse(NextIndiceResponse value) {
        return new JAXBElement<NextIndiceResponse>(_NextIndiceResponse_QNAME, NextIndiceResponse.class, null, value);
    }

}
